<?php
/**
 * wecoders functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wecoders
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'wecoders_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wecoders_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on wecoders, use a find and replace
		 * to change 'wecoders' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wecoders', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'wecoders' ),
				'menu-2' => esc_html__( 'Secondary', 'wecoders' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'wecoders_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'wecoders_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wecoders_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wecoders_content_width', 640 );
}
add_action( 'after_setup_theme', 'wecoders_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wecoders_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Blog sidebar', 'wecoders' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'wecoders' ),
			'before_widget' => '<section id="%1$s" class="widget mb-60 %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="sidebar-title text-uppercase mb-35 pb-10">',
			'after_title'   => '</h4>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Blog Map', 'wecoders' ),
			'id'            => 'sidebar-2',
			'description'   => esc_html__( 'Add widgets here.', 'wecoders' ),
			'before_widget' => '<section id="%1$s" class="mb-60 %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '',
			'after_title'   => '',
		)
	);

	register_widget( 'WeCoders_Widget_Search' );

	register_widget( 'WeCoders_Widget_Categories');

	register_widget( 'WeCoders_Widget_Tags');
}
add_action( 'widgets_init', 'wecoders_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wecoders_scripts() {
	wp_enqueue_style( 'wecoders-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'wecoders-style', 'rtl', 'replace' );

	wp_enqueue_style( 'wecoders-fontg', 'https://fonts.googleapis.com/css?family=Lato:300,400,700' );
	wp_enqueue_style( 'wecoders-fonta', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css' );

	wp_enqueue_style( 'wecoders-boots', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-owlca', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-anima', get_template_directory_uri() . '/assets/css/animate-text.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-magni', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-etlin', get_template_directory_uri() . '/assets/css/et-line.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-peico', get_template_directory_uri() . '/assets/css/pe-icon-7-stroke.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-short', get_template_directory_uri() . '/assets/css/shortcode/shortcodes.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-meanm', get_template_directory_uri() . '/assets/css/meanmenu.min.css', array(), _S_VERSION );
	// wp_enqueue_style( 'wecoders-fonta', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-respo', get_template_directory_uri() . '/assets/css/responsive.css', array(), _S_VERSION );
	wp_enqueue_style( 'wecoders-mains', get_template_directory_uri() . '/assets/style.css', array(), _S_VERSION );

	wp_enqueue_script( 'wecoders-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js' );
    wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'wecoders-moder', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array(), _S_VERSION, false );
	wp_enqueue_script( 'wecoders-btsjs', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-owljs', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-count', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-wayjs', get_template_directory_uri() . '/assets/js/waypoints.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-magni', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-mixit', get_template_directory_uri() . '/assets/js/jquery.mixitup.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-meajs', get_template_directory_uri() . '/assets/js/jquery.meanmenu.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-navjs', get_template_directory_uri() . '/assets/js/jquery.nav.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-paral', get_template_directory_uri() . '/assets/js/jquery.parallax-1.1.3.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-anijs', get_template_directory_uri() . '/assets/js/animate-text.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-plgjs', get_template_directory_uri() . '/assets/js/plugins.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'wecoders-mainj', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wecoders_scripts' );

require get_template_directory() . '/inc/custom-menu.php';

require get_template_directory() . '/inc/custom-pagination.php';

require get_template_directory() . '/inc/helpers.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/inc/widgets/widget-search.php';

require get_template_directory() . '/inc/widgets/widget-categories.php';

require get_template_directory() . '/inc/widgets/widget-tags.php';