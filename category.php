<?php
get_header();

$title = single_cat_title( __( 'Category', 'wecoders' ) . ': ', false );
?>

<?php get_template_part( 'template-parts/breadcrumbs', '', ['title' => $title] ) ?>

<?php get_template_part( 'template-parts/content', 'posts' ) ?>

<?php
get_footer();