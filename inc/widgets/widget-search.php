<?php

class WeCoders_Widget_Search extends WP_Widget
{
    public function __construct()
    {
        parent::__construct( 
            'WeCoders_Widget_Search', 
            'WeCoders - Widget Search', 
            [
                'name'        => __( 'WeCoders - Search', 'wecoders' ),
                'description' => __( 'Input widget search', 'wecoders' )
            ] 
        );
    }

    public function form( $instance )
    {
        $title = @ $instance['title'] ?: '';

        $output = '<p>';
        $output .= '<label for="' . $this->get_field_id( 'title' ) . '">' . __( 'Title:' ) . '</label>'; 
        $output .= '<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' .  esc_attr( $title ) . '">';
        $output .= '</p>';

        echo $output;
    }

    public function update( $new_instance, $old_instance )
    {
        $instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;        
    }

    public function widget( $args, $instance )
    {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];

        $output = '<form class="search-form" action="/" method="GET">';
        $output .= '<input type="text" name="s" placeholder="' . $title . '"/>';
        $output .= '<button><i class="fa fa-search"></i></button>';
        $output .= '</form>';

        echo $output;
		
        echo $args['after_widget'];
    }
}