<?php

class WeCoders_Widget_Categories extends WP_Widget
{
    public function __construct()
    {
        parent::__construct( 
            'WeCoders_Widget_Categories', 
            'WeCoders - Widget Categories', 
            [
                'name'        => __( 'WeCoders - Categories', 'wecoders' ),
                'description' => __( 'Input widget Categories', 'wecoders' )
            ] 
        );
    }

    public function form( $instance )
    {
        $title = @ $instance['title'] ?: '';

        $output = '<p>';
        $output .= '<label for="' . $this->get_field_id( 'title' ) . '">' . __( 'Title:' ) . '</label>'; 
        $output .= '<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' .  esc_attr( $title ) . '">';
        $output .= '</p>';

        echo $output;
    }

    public function update( $new_instance, $old_instance )
    {
        $instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;        
    }

    public function widget( $args, $instance )
    {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];

        echo '<h4 class="sidebar-title text-uppercase mb-35 pb-10">' . esc_attr( $title ) . '</h4>';

        $output = '<ul class="widget-cat">';

        $categories = get_categories( [
            'taxonomy'     => 'category',
            'type'         => 'post',
            'child_of'     => 0,
            'parent'       => '',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => 0,
            'hierarchical' => 1,
            'exclude'      => '',
            'include'      => '',
            'number'       => 0,
            'pad_counts'   => false,
        ] );
        
        if( $categories ) {
            foreach( $categories as $cat ) {
                $output .= '<li>';
                $output .= '<a href="/category/' . $cat->slug . '">' . $cat->name . '</a>';
                $output .= '</li>';
            }
        }

        $output .= '</ul>';

        echo $output;
		
        echo $args['after_widget'];
    }
}