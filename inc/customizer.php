<?php
/**
 * wecoders Theme Customizer
 *
 * @package wecoders
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wecoders_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'wecoders_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'wecoders_customize_partial_blogdescription',
			)
		);
	}

	// Email 
	$setting = 'wecoders_email';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Email', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	if ( isset( $wp_customize->selective_refresh ) ){

		$wp_customize->selective_refresh->add_partial( $setting, [
			'selector'            => '.welcome .email',
			'container_inclusive' => false,
			'render_callback'     => 'footer_inner_dh5theme',
			'fallback_refresh'    => false,
		] );
	}

	// Phone 
	$setting = 'wecoders_phone';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> 'No phone',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Phone', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	if ( isset( $wp_customize->selective_refresh ) ){

		$wp_customize->selective_refresh->add_partial( $setting, [
			'selector'            => '.welcome .phone',
			'container_inclusive' => false,
			'render_callback'     => 'footer_inner_dh5theme',
			'fallback_refresh'    => false,
		] );
	}

	// Address 
	$setting = 'wecoders_address';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> 'Your address',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Address', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// Facebook 
	$setting = 'wecoders_facebook';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Facebook', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// Twitter 
	$setting = 'wecoders_twitter';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Twitter', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// Instagram 
	$setting = 'wecoders_instagram';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Instagram', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// youtube 
	$setting = 'wecoders_youtube';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Youtube', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// linkedin 
	$setting = 'wecoders_linkedin';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Linkedin', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// telegram 
	$setting = 'wecoders_telegram';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Telegram', 'wecoders' ),
			'type'		=> 'text'
		]
	);

	// tiktok 
	$setting = 'wecoders_tiktok';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'refresh'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'title_tagline',
			'label'		=> __( 'Tiktok', 'wecoders' ),
			'type'		=> 'text'
		]
	);
}
add_action( 'customize_register', 'wecoders_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function wecoders_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function wecoders_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wecoders_customize_preview_js() {
	wp_enqueue_script( 'wecoders-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'wecoders_customize_preview_js' );
