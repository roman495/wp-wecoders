<?php

function dd( $data ) {
    $result = '<pre style="background:#2c3e50;color:#27ae60;font-size:1.1em;font-family:\'JetBrains Mono\';text-align:left;padding:20px;">';
    $result .= print_r( $data, true );
    $result .= '</pre>';
    echo $result;
}

function phone_number( $phone ) {
    $phone = str_replace( ' ', '', $phone );
    $phone = str_replace( '-', '', $phone );
    $phone = str_replace( '(', '', $phone );
    $phone = str_replace( ')', '', $phone );
    $phone = str_replace( '+', '', $phone );

    return 'tel:+' . $phone;
} 