<?php

function wecoders_navigation_template( $template, $class ) {
    return '<nav role="navigation">%3$s</nav>';
}
add_filter( 'navigation_markup_template', 'wecoders_navigation_template', 10, 2 );
