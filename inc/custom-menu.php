<?php

add_filter( 'nav_menu_submenu_css_class', 'filter_menu_1', 10, 3 );
function filter_menu_1( $classes, $args, $depth ) {
	if ( $args->container_id == 'primary-menu' ) {
		$classes[] = 'dropdown';
	}
	return $classes;
}

add_filter( 'nav_menu_item_title', 'filter_nav_menu_item_title', 10, 4 );
function filter_nav_menu_item_title( $title, $item, $args, $depth ) {
    if ( 
        $args->container_id == 'primary-menu'
        && in_array( 'menu-item-has-children', $item->classes ) 
    ) {
        $title .= ' <span class="indicator"><i class="fa fa-angle-down"></i></span>';
    }
	return $title;
}
