<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wecoders
 */

get_header();
?>

<?php get_template_part( 'template-parts/breadcrumbs', '', [ 'title' => get_the_title() ] ) ?>

<div class="blog-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
				<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', get_post_type() );

						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
				?>

                <!-- Комментарии -->
                <h3 class="total-comments mb-30 pb-15">4 комментария</h3>
                <ul class="media-list comment-list mt-30">
                    <!-- Коммент 1 -->
                    <li class="media">
                        <div class="media-body">
                            <div class="comment-info">
                                <h4 class="author-name">Алексей Потапов</h4>
                                <div class="comment-meta">
                                    <a href="#" class="comment-report-link">Ответить</a>
                                </div>
                            </div>
                            <p>Система управления содержимым веб-сайтов Битрикс (с 2007 года используется название "1С-Битрикс") выпущена как отчуждаемый от разработчиков продукт в 2002 году.
                               30 мая 2018 года выпущена последняя на данный момент версия ядра под номером 18.0.0.</p>
                        </div>
                    </li>

                    <!-- Коммент 2 -->
                    <li class="media">
                        <div class="media-body">
                            <div class="comment-info">
                                <h4 class="author-name">Александр М.</h4>
                            </div>
                            <p>Отличное уточнение!</p>
                        </div>
                    </li>
                </ul>

                <!-- Форма для ввода комментария -->
                <div class="comments-form mt-40">
                    <div class="row">
                        <form action="#">
                            <div class="col-md-6 mb-30">
                                <input type="text" placeholder="Ваше имя"/>
                            </div>
                            <div class="col-md-6 mb-30">
                                <input type="email" placeholder="Email"/>
                            </div>
                            <div class="col-md-12">
                                <textarea name="message" cols="30" rows="3" placeholder="Комментарий"></textarea>
                                <button class="btn btn-lg mt-30">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12 mt-sm-40 mt-xs-40">
				<?php
					if ( is_active_sidebar( 'sidebar-1' ) ) {
						dynamic_sidebar( 'sidebar-1' );
					}
				?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
