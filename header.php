<!DOCTYPE html>
<html class="no-js" <?php language_attributes() ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" type="image/x-icon" href="<?= get_template_directory_uri() ?>/assets/img/favicon.png">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<!-- Линия с контактами -->
<?php if( ! is_front_page() ) : ?>
    <div class="header-top-area bg-color ptb-10 hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="welcome">
                        <span><i class="fa fa-envelope"></i> <a href="mailto:<?= get_theme_mod( 'wecoders_email' ) ?>" class="email"><?= get_theme_mod( 'wecoders_email' ) ?></a></span>
                        <span><i class="fa fa-phone"></i> <a href="<?= phone_number( get_theme_mod( 'wecoders_phone' ) ) ?>" class="phone"><?= get_theme_mod( 'wecoders_phone' ) ?></a></span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="social-icon-header text-right">
                        <a href="<?= get_theme_mod( 'wecoders_facebook' ) ?>"><i class="fab fa-facebook"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_twitter' ) ?>"><i class="fab fa-twitter"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_linkedin' ) ?>"><i class="fab fa-linkedin"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_telegram' ) ?>"><i class="fab fa-telegram-plane"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_youtube' ) ?>"><i class="fab fa-youtube"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_tiktok' ) ?>"><i class="fab fa-tiktok"></i></a>
                        <a href="<?= get_theme_mod( 'wecoders_instagram' ) ?>"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<!-- Шапка сайта (меню) -->
<header id="sticky-header" class="header-area header-wrapper <?= is_front_page() ? 'transparent-header' : 'white-bg' ?>">

    <!-- Меню (для десктопа) -->
    <div class="header-middle-area full-width">
        <div class="container">
            <div class="full-width-mega-dropdown">
                <div class="row">
                    <!-- Логотип -->
                    <div class="col-md-2 col-sm-3 col-xs-8">
                        <div class="logo ptb-22">
                            <a href="<?php echo home_url() ?>">
                                <?php the_custom_logo(); ?>
                            </a>
                        </div>
                    </div>

                    <!-- Меню (основное) -->
                    <div class="col-md-10 col-sm-9 col-xs-4 text-right">
                        <div class="header-main-menu hidden-xs">
                            <?php 
                                wp_nav_menu( [
                                    'theme_location'  => 'menu-1',
                                    'container'       => 'nav', 
                                    'container_id'    => 'primary-menu',
                                    'menu_class'      => 'main-menu text-right', 
                                    'echo'            => true,
                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                ] );
                            ?>
                        </div>

                        <!-- Поиск -->
                        <div class="header-right">
                            <div class="header-search">
                                <div class="search-wrapper">
                                    <a href="javascript:void(0);" class="search-open">
                                        <i class="pe-7s-search"></i>
                                    </a>
                                    <div class="search-inside animated bounceInUp">
                                        <i class="icon-close search-close fa fa-times"></i>
                                        <div class="search-overlay"></div>
                                        <div class="position-center-center">
                                            <div class="search">
                                                <form>
                                                    <input type="search" placeholder="Поиск по сайту">
                                                    <button type="submit"><i class="fa fa-search"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Меню (для мобилки) -->
    <div class="mobile-menu-area visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <?php 
                            wp_nav_menu( [
                                'theme_location'  => 'menu-1',
                                'container'       => 'nav', 
                                'container_id'    => 'dropdown',
                                'menu_class'      => '', 
                                'echo'            => true,
                                'items_wrap'      => '<ul>%3$s</ul>',
                                'depth'           => 0,
                            ] );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
