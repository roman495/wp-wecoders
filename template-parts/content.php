<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wecoders
 */

?>

<article class="post-wrapper mb-60">
	<h1 class="text-blue"><?php the_title() ?></h1>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-img hover-bg-opacity">
			<a href="<?php the_permalink() ?>">
				<?php the_post_thumbnail( 'full' ) ?>
			</a>
		</div>
	<?php endif ?>
	<div class="post-content">
		<div class="post-meta">
			<span><a href="#!"><i class="fa fa-clock-o"></i> <?php the_date() ?></a></span> -
			<span><a href="#!"><i class="fa fa-user"></i> <?php the_author() ?></a></span> -
			<?php 
				$cats = get_the_category();
				foreach( $cats as $cat ) : 
			?>
				<span><a href="/category/<?= $cat->slug ?>/"><?= $cat->name ?></a></span>
				<?php if( next( $cats ) ) : ?>
					&nbsp;-
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<?php the_content() ?>
	</div>
</article>
