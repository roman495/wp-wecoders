<div class="blog-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>
                    <!-- Запись блога 1 -->
                    <article class="post-wrapper mb-60">
                        <?php if ( has_post_thumbnail() ) : ?>
                            <div class="post-img hover-bg-opacity">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_post_thumbnail( 'full' ) ?>
                                </a>
                            </div>
                        <?php endif ?>
                        <div class="post-content">
                            <h3>
                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                            </h3>
                            <div class="post-meta">
                                <span><a href="<?php the_permalink() ?>"><i class="fa fa-clock-o"></i> <?php echo get_the_date() ?></a></span> -
                                <span><a href="#"><i class="fa fa-user"></i> <?php the_author() ?></a></span> -
                                <?php
                                    $first_cat = get_the_category()[0];
                                ?>
                                <span><a href="/category/<?= $first_cat->slug ?>/"><?= $first_cat->name ?></a></span> -
                                <span><a href="<?php the_permalink() ?>"><i class="fa fa-comments"></i> <?php comments_number( ); ?></a></span>
                            </div>
                            <?php the_excerpt() ?>
                            <a class="read-more btn btn-small" href="<?php the_permalink() ?>"><?php _e( 'Read more', 'wecoders' ) ?>
                                <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </article>
                <?php endwhile ?>
                    <?php
                        $paginate_links = paginate_links( [
                            'type'      => 'array',
                            'prev_text' => '<i class="fas fa-chevron-left"></i>',
                            'next_text' => '<i class="fas fa-chevron-right"></i>',                
                        ] );
                        if ( ! is_null( $paginate_links ) ) {
                            $output = '<ul class="pagination mt-50">';
                            foreach ( $paginate_links as $page_link ) {
                                $output .= '<li>' . $page_link . '</li>';
                            }
                            $output .= '</ul>';
                            echo $output;
                        }
                    ?>
                <?php else : ?>
                    <p><?php _e( 'No posts', 'wecoders' ) ?></p>
                <?php endif ?>
            </div>

            <!-- Правая колонка -->
            <div class="col-md-4 col-sm-12 col-xs-12 mt-sm-40 mt-xs-40">
                <?php
                    if ( is_active_sidebar( 'sidebar-1' ) ) {
                        dynamic_sidebar( 'sidebar-1' );
                    }
                ?>
            </div>
        </div>
    </div>
</div>
