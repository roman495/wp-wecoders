<!-- Хлебные крошки (навигация) -->
<div class="breadcrumb-area brand-bg ptb-100">
    <div class="container width-100">
        <div class="row z-index">
            <div class="col-md-7 col-sm-6">
                <div class="breadcrumb-title">
                    <h2 class="white-text"><?php echo $args['title'] ?></h2>
                </div>
            </div>
            <div class="col-md-5 col-sm-6">
                <div class="breadcrumb-menu">
                    <ol class="breadcrumb text-right">
                        <li>
                            <a href="<?php echo home_url() ?>">Главная</a>
                        </li>
                        <li class="active">
                            <a href="#">Блог</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
