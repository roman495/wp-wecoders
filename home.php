<?php
get_header();
?>

<?php get_template_part( 'template-parts/breadcrumbs', '', ['title' => __( 'Blog', 'wecoders' )] ) ?>

<?php get_template_part( 'template-parts/content', 'posts' ) ?>

<?php
get_footer();